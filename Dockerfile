FROM ubuntu:latest

 MAINTAINER Gokhan Sengun <gokhansengun@gmail.com>

 # Paket listelerini download et
 RUN apt-get update

 # Nginx paketini yükle
 RUN apt-get install -y nginx

 ADD [ "./", "/var/www/html/" ]

 EXPOSE 80

 ENTRYPOINT nginx -g 'daemon off;'
